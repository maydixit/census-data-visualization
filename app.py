from flask import Flask, render_template, jsonify
from data_reader import get_data, get_columns, get_omit_count

app = Flask(__name__)


@app.route("/")
def index():
    return render_template("index.html")

@app.route("/omit_count/<column>")
def omit_count(column):
    return jsonify(get_omit_count(column))

@app.route("/data/<column>")
def data(column):
    return jsonify(get_data(column))

@app.route("/columns")
def columns():
    return jsonify(get_columns())


if __name__ == "__main__":
    app.run(debug=True)
