"""
python file that provides an interface to the sqlite database
contains predetermined valuse for: database name, tablename, column to average by, limit.
Author: May Dixit
"""
import sqlite3
from flask import g
import sys

DATABASE = 'us-census.db'
TABLENAME = 'census_learn_sql'
AVG_COL = "age"
LIMIT = 100

'''
Function that takes in a table name or the default and returns all the column names in this table.
input: table name
output: column name list as a dictionary of format: {"columns": list}
'''
def get_columns(table_name=TABLENAME):
    db = get_db()
    table_name = get_table_name(db)
    out = query_db('PRAGMA table_info( ' + table_name + ');');
    ret = list()
    for val in out:
        ret.append(val[1])
    return {"columns":ret}

'''
Function that makes a dictionary from a given list and headers.
input: rows - rows to be converted to a dictionary
       headers - the header values
output: dictionary of the format: {'data': [ { headers[0]: row[0][0] , headers[1]: row[0][1]...} ,
            {headers[0]: row[1][0] , headers[1]: row[1][1]...} , ... ], 'headers': headers}
'''
def make_dict(rows, headers):
    dlist = list()
    for row in rows:
        d = dict()
        for (header, val) in zip(headers, row):
            d[header] = val
        dlist.append(d)
    return {"data": dlist, 'headers': headers}
'''
Function that returns the data required for a given column and based on the arguments provided.
input: colName - column name
       args - stub to add support for more ways of fetching a data
output: a dictionary of the format: {'data' :[ { headers[0]: row[0][0] , headers[1]: row[0][1]...} ,
            {headers[0]: row[1][0] , headers[1]: row[1][1]...} , ... ], 'headers': headers }
            where headers are Column Name, Count of rows with each value in this column, Average age
            per values (in the default case).
        the data is grouped by the column, ordered by count
'''
def get_data(colName, args= None):
    db = get_db()
    table_name = get_table_name(db)
    query = "select [{}] , count(1) as valCount, Avg( [{}]) as averagAge from [{}] group by [{}] order by valCount desc LIMIT {};".format(colName, AVG_COL, table_name\
            , colName, str(LIMIT))
    results = query_db(query)
    return make_dict(results, [str(colName) ,"count" ,"Average Age"])

'''
Stub to change table names
'''
def get_table_name(db , args = None):
    #can be extended to select a particular table. args can specify which table
    return TABLENAME

'''
Function to get counts of total rows and total values in the column omitted.
Input: colName - column name, table_name - table name
Output: dictionary of the format: {"count_vals" : count of values in the column omitted, "count_rows": count of total rows in the table omitted}
'''
def get_omit_count(colName, table_name=TABLENAME):
    query1 = "select count(1) as ct from (select distinct ([{}]) from [{}]) " .format(colName, table_name)
    count_vals = query_db(query1)
    query2 = "select count(1) from [{}];".format(table_name)
    count_rows = query_db(query2)
    return  {"count_vals": count_vals, "count_rows" : count_rows}

'''
Helper method to get database object
returns a database object
'''
def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
    return db

'''
Helper method to run a query
returns the results of the query or None
'''
def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv

'''
Helper method to close connection to the DB
'''
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()
