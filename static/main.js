$(function() {
  console.log('jquery is working!');
  createMenu();
});

LIMIT = 100
/*
  function to create the menu element by fetching columns from the database
*/
function createMenu(){
    var scrollList = document.getElementById("scrollmenu")
    scrollList.style.height = 8*window.innerHeight/10+"px"

    d3.json("/columns", function(error, quotes) {
      columns = quotes['columns']
      for (index in columns){
        var a = document.createElement("a");
        var text = document.createTextNode(columns[index]);
        a.appendChild(text);
        a.addEventListener("click", function(){ updateData(this.innerHTML);});
        scrollList.appendChild(a);
      }
    });
}

/*
  function to update the data everytime there is an event by the user.
  input @column : takes the column to fetch the content for.
  fetches the data from the database for the column and displays it into the table.
*/
function updateData(column){
  $.getJSON('/data/'+column, {}, function(data){

    //fetch more information from the database only for the cases where any part of the input was
    //omitted.
    if(data['data'].length < LIMIT){
      var display_count = data['data'].length;
      var total_vals = display_count;
      var omitted_row_count = 0;
      document.getElementById("row_count").innerHTML = "Displaying " + display_count  + " of " + total_vals +  " values.";
      document.getElementById("total_count").innerHTML = "Omitted " + omitted_row_count + " rows.";
    }
    else{
      $.getJSON('/omit_count/'+column, {}, function(counts){
        var display_count = LIMIT;
        var total_vals = counts["count_vals"];
        var omitted_row_count = counts["count_rows"][0];
        for (data_val in data['data']){
          omitted_row_count -= data['data'][data_val]['count'];
        }
        document.getElementById("row_count").innerHTML = "Displaying " + display_count  + " of " + total_vals +  " values.";
        document.getElementById("total_count").innerHTML = "Omitted " + omitted_row_count + " rows.";
      })

    }

    d3.select('#visualization').selectAll("*").remove();
    var table = d3.select('#visualization').append('table');
    var thead = table.append('thead')
    var	tbody = table.append('tbody');
    var sortAscending = true;
    // append the header row
		thead.append('tr')
		  .selectAll('th')
		  .data(data['headers']).enter()
		  .append('th')
		    .text(function (column) { return column; })
        .on('click', function (d) {
		                	   thead.attr('class', 'header');
		                	   if (sortAscending) {
		                	     rows.sort(function(a, b) { return b[d] < a[d]; });
		                	     sortAscending = false;
		                	     this.className = 'aes';
		                	   } else {
		                		 rows.sort(function(a, b) { return b[d] > a[d]; });
		                		 sortAscending = true;
		                		 this.className = 'des';
		                	   }
		                   });;
    //append the rest of the rows
    var rows = tbody.selectAll('tr')
    		  .data(data['data'])
    		  .enter()
    		  .append('tr');
    //append cells and add their content
    var cells = rows.selectAll('td')
          .data(function(row) {
            return data['headers'].map(function (column) {
		                return {column: column, value: row[column]};
		        });
          })
          .enter()
          .append('td')
          .text(function(d) {
              if (d.value == null)
                return "Null";
              return d.value;
            });
  });
}
